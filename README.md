# A C++ library with a collection of IMU Calibration algorithms

## Contributors

* Emil Fresk

---

## License

Licensed under the LGPL-v3 license, see LICENSE file for details.

