/****************************************************************************
*
* Copyright (C) 2015-2016 Emil Fresk.
* All rights reserved.
*
* This file is part of the IMU Calibration library.
*
* GNU Lesser General Public License Usage
* This file may be used under the terms of the GNU Lesser
* General Public License version 3.0 as published by the Free Software
* Foundation and appearing in the file LICENSE included in the
* packaging of this file.  Please review the following information to
* ensure the GNU Lesser General Public License version 3.0 requirements
* will be met: http://www.gnu.org/licenses/lgpl-3.0.html.
*
* If you have questions regarding the use of this file, please contact
* Emil Fresk at emil.fresk@gmail.com.
*
****************************************************************************/

/* System includes. */
#include <cmath>
#include <iostream>
#include <vector>

/* Data includes. */

/* Processing includes. */
#include <Eigen/Eigen>

#ifndef _ELLIPSOID_CALIBRATION_H
#define _ELLIPSOID_CALIBRATION_H

namespace IMUCalibration
{
    template <typename T>
    class EllipsoidCalibration;
}


/**
 * @brief An implementation of an ellipsoid parameter estimation algorithm
 *        that can be used to calibrate accelerometers and magnetometers.
 */
template <typename T>
class IMUCalibration::EllipsoidCalibration
{

private:

    /**
     * @brief Typedefs to keep the code readable.
     */
    typedef Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> CalMatrix;
    typedef Eigen::Matrix<T, Eigen::Dynamic, 1> CalVector;

public:

    /**
     * @brief Estimates the bias and the gain of an ellipsoid model for sensor
     *        calibration of accelerometers or magnetometers.
     *
     * @details The parameters estimated are then used as:
     *          x_comp = (x_m - x_bias) * x_gain
     *
     * @params[in]  x_measurements  Vector holding the x-axis measurements.
     * @params[in]  y_measurements  Vector holding the y-axis measurements.
     * @params[in]  z_measurements  Vector holding the z-axis measurements.
     * @params[out] bias_out        Pointer to the store location of the bias.
     * @params[out] gain_out        Pointer to the store location of the gain.
     */
    static void EstimateParameters(const std::vector<T> &x_measurements,
                                   const std::vector<T> &y_measurements,
                                   const std::vector<T> &z_measurements,
                                   Eigen::Matrix<T, 3, 1> *bias_out,
                                   Eigen::Matrix<T, 3, 1> *gain_out)
    {
        /* Check the input parameters. */
        eigen_assert(x_measurements.size() == y_measurements.size());
        eigen_assert(x_measurements.size() == z_measurements.size());
        eigen_assert(y_measurements.size() == z_measurements.size());

        const int size = x_measurements.size();

        /* Vector for the input data. */
        CalVector x(size, 1), y(size, 1), z(size, 1);

        /* Fill the vectors with a copy of the data. */
        x = CalVector::Map(x_measurements.data(), size);
        y = CalVector::Map(y_measurements.data(), size);
        z = CalVector::Map(z_measurements.data(), size);

        /* Create the input parameter matrix and vector. */
        CalMatrix A(size, 6);
        CalMatrix b(size, 1);

        A.col(0) = x.cwiseProduct(x) + y.cwiseProduct(y) - 2 * z.cwiseProduct(z);
        A.col(1) = x.cwiseProduct(x) + z.cwiseProduct(z) - 2 * y.cwiseProduct(y);
        A.col(2) = 2 * x;
        A.col(3) = 2 * y;
        A.col(4) = 2 * z;
        A.col(5).setOnes();

        b = x.cwiseProduct(x) + y.cwiseProduct(y) + z.cwiseProduct(z);

        /* Solve the Least-Squares problem using a stable method (SVD). */
        Eigen::Matrix<T, 6, 1> u =
            A.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(b);

        /* Convert parameters for bias and gain calculations. */
        Eigen::Matrix<T, 3, 1> v1(- u(0) - u(1) + 1,
                                  - u(0) + 2 * u(1) + 1,
                                  - u(1) + 2 * u(0) + 1);

        Eigen::Matrix<T, 3, 1> v2(u(2), u(3), u(4));

        T c = u(5);

        /* Calculate bias and return. */
        *bias_out = v2.cwiseQuotient(v1);

        /* Calculate gain. */
        *gain_out = ( v1 / ((*bias_out).transpose() * v2 + c) ).cwiseSqrt();
    }

};

#endif
